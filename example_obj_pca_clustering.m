%%%% example: PCA + K-means++ clustering, visualizing in 2D
clear all; clc; 
load('MatFiles\BodyMap20D_andObjectData.mat'); 
X = Astar.itemBody;  

% code labels
categs = Astar.categName;
labels = unique(categs);
y = zeros(size(X, 1), 1); 
for c = 1:length(labels)
    y(strcmp(categs, labels(c)), 1) = c; 
end

% PCA
p = 6; % number of dimensions in the reduced space
modelPCA = objectPCA(X, p, y); 
Z = modelPCA.Z; 

%% K-means++
K = 7; % number of clusters
modelKmeansPP = clusterKmeansPlusPlus(Z, K);
Wk = modelKmeansPP.evaluate(modelKmeansPP, Z); 
modelKmeansPP.displayItems(modelKmeansPP, Astar.itemName, Astar.categName, y); 
modelKmeansPP.plotClusters(modelKmeansPP, Astar.itemName, Z); 

%% DBSCAN
eps = 3; 
minPts = 3; 
modelDB = clusterDBcluster(Z, eps, minPts);
Wks = modelDB.evaluate(modelDB, Z);
modelDB.displayItems(modelDB, Astar.itemName, Astar.categName, y); 
modelDB.plotClusters(modelDB, Astar.itemName, Z); 

%% GMM
K = 4; % lowestNLL = 1257.1
bestModel = []; 
lowestNLL = Inf; 

for i = 1:50
    [modelGMM, LL] = clusterGMM(Z, K);
    if LL(end) < lowestNLL
        lowestNLL = LL(end); 
        bestModel = modelGMM; 
    end
end
disp(lowestNLL)
modelGMM = bestModel; 
modelGMM.displayItems(modelGMM, Astar.itemName, Astar.categName, y); 
modelGMM.plotClusters(modelGMM, Astar.itemName, Z); 
