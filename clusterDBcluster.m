function [model] = clusterDBcluster(X, eps, minPts)
% cluster the data using DBSCAN
% inputs: 
%   X - n-by-d data matrix
%   eps - distance threshold
%   minPts - minimum number of points for a cluster
% output: 
%   model - a model that shows the clusters of the training data; has
%       functions 'evaluate', 'showClusters', 'plotClusters', 'displayItems'

[N, D] = size(X);

% Compute distances between all points
D = X.^2*ones(D, N) + ones(N, D)*(X').^2 - 2*X*X';

% This will be the cluster of each object.
cluster = zeros(N, 1);

% This variable will keep track of whether we've visited each object.
visited = zeros(N, 1);

% K will count the number of clusters we've found
K = 0;
for i = 1:N
    if ~visited(i) 
        % We only need to consider examples that have never been visited
        visited(i) = 1;
        neighbors = find(D(:,i) <= eps);
        if length(neighbors) >= minPts 
            % We found a new cluster
            K = K + 1;
            [visited, cluster] = expand(X, i, neighbors, K, eps, minPts, ...
                D, visited, cluster);
        end
    end
end
model.clusters = cluster;
model.K = K; 
model.showClusters = @showClusters; 
model.plotClusters = @plotClusters; 
model.evaluate = @evaluate; 
model.displayItems = @displayItems; 
end

function [visited, cluster] = expand(X, i, neighbors, K, eps, minPts, ...
    D, visited, cluster)
cluster(i) = K;
ind = 0;
while 1
    ind = ind+1;
    if ind > length(neighbors)
        break;
    end
    n = neighbors(ind);
    cluster(n) = K;
    
    if ~visited(n)
        visited(n) = 1;
        neighbors2 = find(D(:, n) <= eps);
        if length(neighbors2) >= minPts
            neighbors = [neighbors; setdiff(neighbors2, neighbors)];
        end
    end    
end
end

function showClusters(model, itemNames, categNames, categOnlyFlag)

num_clusters = max(model.clusters);

% display items in each cluster
for k = 0:num_clusters
    fprintf('Cluster %d: ',k);
    items = itemNames(model.clusters==k);
    categs = categNames(model.clusters==k);
    if categOnlyFlag
        categs_unique = unique(categs);
        for i = 1:length(categs_unique)
            fprintf('%s ', categs_unique{i});
        end
    else
        for i = 1:length(items)
            fprintf('%s (%s) ', items{i}, categs{i});
            if mod(i, 5) == 0
                fprintf('\n');
            end
        end
    end
    fprintf('\n');
end

end

% plot each item in the first two dimensions
function plotClusters(model, itemNames, X)

num_clusters = max(model.clusters);
colors = getColors; 
symbols = getSymbols; 
figure; 
% display items in each cluster
for k = 0:num_clusters
    Xk = X(model.clusters==k, 1:2); 
    h = plot(Xk(:, 1)-0.05, Xk(:, 2), '.');
    set(h, 'Color', colors{k+1}, 'Marker', symbols{k+1});
    
    items = itemNames(model.clusters==k);
    hold on;
    for i = 1:size(Xk, 1)
        t = text(Xk(i, 1), Xk(i, 2), items(i, :));
        set(t, 'Color', colors{k+1}); 
    end
end

end

function displayItems(model, itemNames, categNames, y)

num_clusters = max(model.clusters); 
colors = getColors; 
symbols = getSymbols; 

f = figure; 
axis([0, num_clusters*2+2, 0, 20]); 
start = [0.5, 0];
for k = 0:num_clusters
    items = itemNames(model.clusters==k);
    yc = y(model.clusters==k); 
    hold on;
    for i = 1:length(items)
        h = plot(start(1), start(2), '.');
        set(h, 'Color', colors{yc(i)}, 'Marker', symbols{yc(i)}); 
        t = text(start(1) + 0.05, start(2), items(i, :));
        set(t, 'Color', colors{k+1});
        start(2) = start(2) + 0.5; 
        
        if start(2) >= 20
            start(1) = start(1) + 1; 
            start(2) = 0.5; 
        end
        
    end
    start(1) = start(1) + 1; 
    start(2) = 0.5;     
end

% plot legend
labels = unique(categNames); 
start = [num_clusters*2+1, 0.5];
for c = 1:length(labels)
    h = plot(start(1), start(2), '.'); 
    set(h, 'Color', colors{c}, 'Marker', symbols{c}); 
    t = text(start(1) + 0.05, start(2), labels(c, :));
    start(2) = start(2) + 0.5; 
end

set(gca, 'XTickLabel', [], 'YTickLabel', []); 

end

% evaluate K in terms of the variance quantity Wk
function Wk = evaluate(model, X)
num_clusters = max(model.clusters);
[n, d] = size(X);

nc = zeros(num_clusters, 1); % nc in cluster c
dc2 = zeros(num_clusters, 1); % sum of squared distance in cluster c
for k = 1:num_clusters
    Xc = X(model.clusters==k, :);
    nc(k) = size(Xc, 1);
    if nc(k) == 1
        dc2(k) = 0;
    else
        dist = Xc.^2*ones(d, nc(k)) + ones(nc(k), d)*(Xc').^2 - 2*Xc*Xc';
        dc2(k) = sum(sum(dist))/(2*nc(k)); 
    end
    
end
Wk = sum(dc2);

end