%%%% example: MDS + K-means++
clear all; clc; 
load('MatFiles\BodyMap20D_andObjectData.mat'); 
load('modelMDS.mat'); 
% MDS data
Z = modelMDS.Z; 

%% code labels
categs = Astar.categName;
labels = unique(categs);
y = zeros(size(Z, 1), 1); 
for c = 1:length(labels)
    y(strcmp(categs, labels(c)), 1) = c; 
end

%% K-means++
K = 6; % number of clusters
modelKmeansPP = clusterKmeansPlusPlus(Z, K); 
modelKmeansPP.displayItems(modelKmeansPP, Astar.itemName, Astar.categName, y); 
modelKmeansPP.plotClusters(modelKmeansPP, Astar.itemName, Z); 


%% DBSCAN
eps = 0.17;
minPts = 3; 
modelDB = clusterDBcluster(Z, eps, minPts); 
modelDB.displayItems(modelDB, Astar.itemName, Astar.categName, y); 
modelDB.plotClusters(modelDB, Astar.itemName, Z); 


%% GMM
K = 7; 
bestModel = []; 
lowestNLL = Inf; 

for i = 1:50
    [modelGMM, LL] = clusterGMM(Z, K);
    if LL(end) < lowestNLL
        lowestNLL = LL(end); 
        bestModel = modelGMM; 
    end
end
lowestNLL
modelGMM = bestModel;  
modelGMM.displayItems(modelGMM, Astar.itemName, Astar.categName, y); 
modelGMM.plotClusters(modelGMM, Astar.itemName, Z); 