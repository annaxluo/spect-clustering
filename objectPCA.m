function [model] = objectPCA(X, p, y)
% reduces dimensionality using principal component analysis (PCA)
% inputs: 
%   X - n-by-d data matrix
%   p - dimensionality of the reduced feature space
%   y - labels
% output: 
%   model - a model that shows the clusters of the training data; has
%       functions 'visualize', 'plot2D', and 'residvar' (compute
%       residual variance)

% standardize columns
[n, d] = size(X); 
X = standardizeCols(X); 

% performs feature selection using PCA
[U, S, V] = svd(X);
W = V(:,1:p)';
Z = X*W';

model.W = W; 
model.Z = Z; 
model.p = p; 
model.y = y; 
model.visualize = @visualize; 
model.plot2D = @plot2D; 
model.residvar = @residvar; 

end

function visualize(model, labels)

% plot weights
figure; 
imagesc(model.W); colorbar; 
title('Weight Vectors'); 
xlabel('d original features'); 
ylabel('p reduced features'); 
set(gca, 'Xtick', [1:length(model.W(1, :))]); 
set(gca, 'Xticklabel', labels); 

% plot the reduced feature space
figure; 
imagesc(model.Z); colorbar; 
title('Reconstruction'); 
xlabel('p reduced features'); 
ylabel('n examples'); 

end

% plot first 2 dimensions in reduced space
function plot2D(model)

figure; 
gscatter(model.Z(:, 1), model.Z(:, 2), model.y, [], ...
    'ox+*sdv^<>phox+*sd', [], 'on');
title('First 2 dimensions in p-space'); 
xlabel('D1'); 
ylabel('D2'); 
H = findobj(gcf, 'tag', 'legend'); 
set(H, 'location', 'eastOutside'); 

end

% calculate the resiudal variances
function r = residvar(model, X)
% distance matrix in original space and reduced space
Dx = distanceMatrix(X); 
Dz = distanceMatrix(model.Z); 

r = 1 - (corr(Dx(:), Dz(:)))^2; 

end


