function D = distanceMatrix(X)
%calculates the Euclidean distance betwee each pair of (Xi, Xj)

[n,d] = size(X);

% Compute all distances
D = X.^2*ones(d, n) + ones(n, d)*(X').^2 - 2*X*X';
D = sqrt(abs(D));

end

