function [model] = featSPCA(X, k, lambda)
% reduces dimensionality using sparse principal components (SPCA)
% inputs: 
%   X - n-by-d data matrix
%   k - dimensionality of the reduced feature space
%   lambda - sparsity parameter: coefficient for L1 regularization
% output: 
%   model - a model that shows the clusters of the training data; has
%       functions 'visualize', 'weightComposition', and 'residvar' (compute
%       residual variance)

maxIter = 500; 
stopCriterion = 1e-2; 

[n,d] = size(X);

% Subtract mean
mu = mean(X);
X = X - repmat(mu,[n 1]);

% Initialize W and Z
W = randn(k, d);
Z = randn(n, k);

% the differentiable part of the objective function
f = (1/2)*sum(sum((X-Z*W).^2));
for iter = 1:maxIter
    fOld = f;
    
    % Update Z with L1-regularization
    Z(:) = findMinL1(@funObjZ, Z(:), lambda, 10, 0, X, W); 
    
    % Update W with L1-regularization
    W(:) = findMinL1(@funObjW, W(:), lambda, 10, 0, X, Z); 
    
    f = (1/2)*sum(sum((X-Z*W).^2));
    
    if fOld - f < stopCriterion
        break;
    end
    
end

% compute the compressed representation
Z(:) = findMinL1(@funObjZ, Z(:), lambda, 500, 0, X, W); 

model.mu = mu; 
model.W = W; 
model.Z = Z; 
model.visualize = @visualize; 
model.weightComposition = @weightComposition; 
model.residvar = @residvar; 
end

function [f, g] = funObjW(W, X, Z)
% Resize vector of parameters into matrix
d = size(X, 2);
k = size(Z, 2);
W = reshape(W, [k d]);

% Compute function and gradient
R = X - Z*W;
f = (1/2)*sum(sum(R.^2));
g = -Z'*R;

% Return a vector
g = g(:);
end

function [f, g] = funObjZ(Z, X, W)
% Resize vector of parameters into matrix
n = size(X, 1);
k = size(W, 1);
Z = reshape(Z, [n k]);

% Compute function and gradient
R = X - Z*W;
f = (1/2)*sum(sum(R.^2));
g = -(R*W');

% Return a vector
g = g(:);
end

function visualize(model)
% plot weights
figure(1); 
imagesc(model.W); colorbar; 
title('Weight Vectors'); 
xlabel('d original features'); 
ylabel('k reduced features'); 

% plot the reduced features
figure(2); 
imagesc(model.Z); colorbar; 
title('Reconstruction'); 
xlabel('k reduced features'); 
ylabel('n examples'); 

end

function weightComposition(model, itemNames, categNames, y)

colors = getColors; 
symbols = getSymbols;

for j = 1:size(model.W, 1)
    figure;
    axis([0, 6*2+2, 0, 20]);
    start = [0.5, 0.5];
    
    wj = model.W(j, :);
    for i = 1:6
        items = itemNames(find(wj < i & wj >= i-1)); 
        yc = y(find(wj < i & wj >= i-1)); 
        text(start(1)+0.5, start(2)-1.5, sprintf('%d~%d', i-1, i));         
        hold on;
        for d = 1:length(items)
            h = plot(start(1), start(2), '.');
            set(h, 'Color', colors{yc(d)}, 'Marker', symbols{yc(d)});
            t = text(start(1) + 0.05, start(2), items(d, :));
            set(t, 'Color', colors{i+1});
            start(2) = start(2) + 0.5;
            
            if start(2) >= 20
                start(1) = start(1) + 1;
                start(2) = 0.5;
            end
            
        end
        start(1) = start(1) + 1;
        start(2) = 0.5;
        
    end
    hold on; 
    % plot legend
    labels = unique(categNames);
    start = [6*2, 0.5];
    for c = 1:length(labels)
        h = plot(start(1), start(2), '.');
        set(h, 'Color', colors{c}, 'Marker', symbols{c});
        t = text(start(1) + 0.05, start(2), labels(c, :));
        start(2) = start(2) + 0.5;
    end
    
    set(gca, 'XTickLabel', [], 'YTickLabel', []);
    
end

end

% calculate the resiudal variances
function r = residvar(model, X)

% compute the residual
Xpred = model.Z*model.W; 

r = norm(Xpred-X, 'fro'); 
end
