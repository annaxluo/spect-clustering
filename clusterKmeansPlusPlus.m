function [model] = clusterKmeansPlusPlus(X, K)
% cluster the data using k-means++
% inputs: 
%   X - n-by-d data matrix
%   K - number of clusters
% output: 
%   model - a model that shows the clusters of the training data; has
%       functions 'evaluate', 'showClusters', 'plotClusters', 'displayItems'


[N, D] = size(X);

% K-mean++ initialization
means = X(ceil(rand*N), :); 
for k = 2:K
    % Euclidean distance between data and existing center
    dist = X.^2*ones(D, k-1) + ones(N, D)*(means').^2 - 2*X*means'; 
    
    minDist = min(dist, [], 2); 
    
    % sample proportional to min dist
    i = sampleDiscrete(minDist/sum(minDist)); 
    means(k, :) = X(i, :); 
end

X2 = X.^2*ones(D, K);
iter = 1;
while 1
    means_old = means;
    
    % Compute Euclidean distance between each data point and each mean
    distances = sqrt(X2 + ones(N, D)*(means').^2 - 2*X*means');
    
    % Assign each data point to closest mean
    [~, clusters] = min(distances, [], 2);
    
    % Compute mean of each cluster
    means = zeros(K, D);
    for k = 1:K
        means(k, :) = mean(X(clusters==k, :),1);
    end
    
    %fprintf('Running K-means, difference = %f\n',max(max(abs(means-means_old))));
    
    if max(max(abs(means-means_old))) < 1e-5
        break;
    end
    iter = iter + 1;
end

model.means = means;
model.clusters = clusters;
model.showClusters = @showClusters;
model.evaluate = @evaluate;
model.plotClusters = @plotClusters; 
model.displayItems = @displayItems; 
end

% same from a discrete PMF
function [y] = sampleDiscrete(p, r)

if nargin < 2
    r = rand; 
end

y = find(cumsum(p) > r, 1); 
end

function showClusters(model, itemNames, categNames, categOnlyFlag)

num_clusters = max(model.clusters);

% display items in each cluster
for k = 1:num_clusters
    fprintf('Cluster %d: ',k);
    items = itemNames(model.clusters==k);
    categs = categNames(model.clusters==k);
    if categOnlyFlag
        categs_unique = unique(categs);
        for i = 1:length(categs_unique)
            fprintf('%s ', categs_unique{i});
        end
    else
        for i = 1:length(items)
            fprintf('%s (%s) ', items{i}, categs{i});
            if mod(i, 5) == 0
                fprintf('\n');
            end
        end
    end
    fprintf('\n');
end

end

% plot each item in the first two dimensions
function plotClusters(model, itemNames, X)
num_clusters = max(model.clusters);
colors = getColors; 
figure; 
% display items in each cluster
for k = 1:num_clusters
    Xk = X(model.clusters==k, 1:2); 
    h = plot(Xk(:, 1), Xk(:, 2), '.');
    set(h, 'Color', colors{k});
    
    items = itemNames(model.clusters==k);
    hold on;
    for i = 1:size(Xk, 1)
        t = text(Xk(i, 1)+0.1, Xk(i, 2), items(i, :));
        set(t, 'Color', colors{k}); 
    end
end

end

function displayItems(model, itemNames, categNames, y)

num_clusters = max(model.clusters); 
colors = getColors; 
symbols = getSymbols; 

f = figure; 
axis([0, num_clusters*2+3, 0, 20]); 
start = [0.5, 0];
for k = 0:num_clusters
    items = itemNames(model.clusters==k);
    yc = y(model.clusters==k); 
    hold on;
    for i = 1:length(items)
        h = plot(start(1), start(2), '.');
        set(h, 'Color', colors{yc(i)}, 'Marker', symbols{yc(i)}); 
        t = text(start(1) + 0.05, start(2), items(i, :));
        set(t, 'Color', colors{k+1});
        start(2) = start(2) + 0.5; 
        
        if start(2) >= 20
            start(1) = start(1) + 1; 
            start(2) = 0.5; 
        end
        
    end
    start(1) = start(1) + 1; 
    start(2) = 0.5;     
end

% plot legend
labels = unique(categNames); 
start = [num_clusters*2+2, 0.5];
for c = 1:length(labels)
    h = plot(start(1), start(2), '.'); 
    set(h, 'Color', colors{c}, 'Marker', symbols{c}); 
    t = text(start(1) + 0.05, start(2), labels(c, :));
    start(2) = start(2) + 0.5; 
end

set(gca, 'XTickLabel', [], 'YTickLabel', []); 

end

% evaluate K in terms of the variance quantity Wk
function Wk = evaluate(model, X)
num_clusters = max(model.clusters);
[n, d] = size(X);

nc = zeros(num_clusters, 1); % nc in cluster c
dc2 = zeros(num_clusters, 1); % sum of squared distance in cluster c
for k = 1:num_clusters
    Xc = X(model.clusters==k, :);
    nc(k) = size(Xc, 1);
    if nc(k) == 1
        dc2(k) = 0;
    else
        means = model.means(k, :);
        Xc2 = Xc.^2*ones(d, 1);
        D = Xc2 + ones(nc(k), d)*(means').^2 - 2*Xc*means';
        dc2(k) = sum(D);
    end
    
end
Wk = sum(dc2);

end

