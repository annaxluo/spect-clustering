function [model] = objectISOMAP(X, p, k, y)
% reduces dimensionality using ISOMAP
% inputs: 
%   X - n-by-d data matrix
%   p - dimensionality of the reduced feature space
%   k - number of neighbors to be considered
%   y - labels
% output: 
%   model - a model that shows the clusters of the training data; has
%       functions 'visualize', and 'plot2D'

[n,d] = size(X);

% Compute all distances
distance = X.^2*ones(d, n) + ones(n, d)*(X').^2 - 2*X*X';
distance = sqrt(abs(distance));

% find k neighbors
G = zeros(n, n);
for i = 1:n
    [~, sorted] = sort(distance(:, i));
    neighbors = setdiff(sorted(1:k+1), i);
    for j = neighbors
        G(i, j) = distance(i, j);
        G(j, i) = distance(j, i);
    end
end

% update the distance with the shortest path
D = zeros(n, n);
for i = 1:n
    for j = i+1:n
        D(i, j) = dijkstra(G, i, j); 
        D(j, i) = D(i, j); 
    end
end

% run MDS
% Initialize low-dimensional representation with PCA
[U, S, V] = svd(X);
W = V(:, 1:p)';
Z = X*W';

Z(:) = findMin(@stress, Z(:), 500, 1, D);

model.W = W;
model.Z = Z;
model.k = k;
model.y = y;
model.visualize = @visualize;
model.plot2D = @plot2D;

end

function [f,g] = stress(Z, D)

n = length(D);
p = numel(Z)/n;
Z = reshape(Z, [n p]);

f = 0;
g = zeros(n, p);
for i = 1:n
    for j = i+1:n
        % Objective Function
        Dz = norm(Z(i,:)-Z(j,:));
        s = D(i, j) - Dz;
        f = f + (1/2)*s^2;
        
        % Gradient
        df = s;
        dgi = (Z(i, :)-Z(j, :))/Dz;
        dgj = (Z(j, :)-Z(i, :))/Dz;
        % accumulate the change
        g(i,:) = g(i,:) - df*dgi;
        g(j,:) = g(j,:) - df*dgj;
    end
end
g = g(:);

end

function visualize(model)

% plot the reduced feature space
figure;
imagesc(model.Z); colorbar;
title('Reconstruction');
xlabel('k reduced features');
ylabel('n examples');

end

function plot2D(model)

figure;
gscatter(model.Z(:, 1), model.Z(:, 2), model.y, [], ...
    'ox+*sdv^<>phox+*sd', [], 'on');
title('First 2 dimensions in k-space');
xlabel('D1');
ylabel('D2');
H = findobj(gcf, 'tag', 'legend');
set(H, 'location', 'eastOutside');

end