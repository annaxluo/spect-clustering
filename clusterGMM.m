function [model, LL] = clusterGMM(X, k)
% Clusters the data using GMM model though EM
% inputs: 
%   X - n-by-d data matrix
%   k - number of clusters
% outputs: 
%   model - a model that shows the clusters of the training data; has
%       functions 'evaluate', 'showClusters', 'plotClusters', 'displayItems'
%   LL - the log-likelihood of the model

tol = 1e-6; 
maxIter = 500; 
[N, D] = size(X); 

% initilization
LL = -Inf(1, maxIter); % log-lik
% random init
y = ceil(k*rand(N, 1)); % randomly choose a cluster for each example
R = full(sparse(1:N, y, 1, N, k, N)); 

% EM
for i = 2:maxIter
    [~, y(:)] = max(R, [], 2); 
    R = R(:, unique(y)); 
    % M-step
    model = maximization(X, R, y);
    % E-step
    [R, LL(i)] = expectation(X, model);
    
    % stopping criterion
    if abs(LL(i)-LL(i-1)) < tol*abs(LL(i))
        break; 
    end    
end
LL = LL(2:i); 

end

% E-step
function [R, ll] = expectation(X, model)

mu = model.mu; % mean
sigma = model.sigma; % variance
theta = model.theta; % class prob

[N, D] = size(X); 
k = size(mu, 2); 
R = zeros(N, k); 
for i = 1:k
    R(:, i) = loggausspdf(X', mu(:, i), sigma(:, :, i)); 
end
R = bsxfun(@plus, R, log(theta)); 

% the log-lik
T = logsumexp(R, 2); 
ll = -sum(T);
R = exp(bsxfun(@minus, R, T)); 

end

% M-step
function model = maximization(X, R, y)

[N, D] = size(X); 
k = size(R, 2); 
Nk = sum(R, 1); 
theta = Nk./N; 
mu = (X'*R).*repmat(1./Nk, D, 1); 

sigma = zeros(D, D, k); 
r = sqrt(R); 
for c = 1:k
    Xo = bsxfun(@minus, X', mu(:, c)); 
    Xo = bsxfun(@times, Xo, r(:, c)'); 
    sigma(:, :, c) = Xo*Xo'/Nk(c) + eye(D)*(1e-6); 
end

model.mu = mu; 
model.sigma = sigma; 
model.theta = theta;
model.clusters = y; % the predicted label
model.K = max(y); 
model.showClusters = @showClusters; 
model.plotClusters = @plotClusters; 
model.displayItems = @displayItems; 
end

function displayItems(model, itemNames, categNames, y)

num_clusters = max(model.clusters); 
colors = getColors; 
symbols = getSymbols; 

f = figure; 
axis([0, num_clusters*2+2, 0, 20]); 
start = [0.5, 0];
for k = 0:num_clusters
    items = itemNames(model.clusters==k);
    yc = y(model.clusters==k); 
    hold on;
    for i = 1:length(items)
        h = plot(start(1), start(2), '.');
        set(h, 'Color', colors{yc(i)}, 'Marker', symbols{yc(i)}); 
        t = text(start(1) + 0.05, start(2), items(i, :));
        set(t, 'Color', colors{k+1});
        start(2) = start(2) + 0.5; 
        
        if start(2) >= 20
            start(1) = start(1) + 1; 
            start(2) = 0.5; 
        end
        
    end
    start(1) = start(1) + 1; 
    start(2) = 0.5;     
end

% plot legend
labels = unique(categNames); 
start = [num_clusters*2+1, 0.5];
for c = 1:length(labels)
    h = plot(start(1), start(2), '.'); 
    set(h, 'Color', colors{c}, 'Marker', symbols{c}); 
    t = text(start(1) + 0.05, start(2), labels(c, :));
    start(2) = start(2) + 0.5; 
end

set(gca, 'XTickLabel', [], 'YTickLabel', []); 

end

function showClusters(model, itemNames, categNames, categOnlyFlag)

num_clusters = max(model.clusters);

% display items in each cluster
for k = 1:num_clusters
    fprintf('Cluster %d: ',k);
    items = itemNames(model.clusters==k);
    categs = categNames(model.clusters==k);
    if categOnlyFlag
        categs_unique = unique(categs);
        for i = 1:length(categs_unique)
            fprintf('%s ', categs_unique{i});
        end
    else
        for i = 1:length(items)
            fprintf('%s (%s) ', items{i}, categs{i});
            if mod(i, 5) == 0
                fprintf('\n');
            end
        end
    end
    fprintf('\n');
end

end

% plot each item in the first two dimensions
function plotClusters(model, itemNames, X)
num_clusters = max(model.clusters);
colors = getColors; 
figure; 
% display items in each cluster
for k = 1:num_clusters
    Xk = X(model.clusters==k, 1:2); 
    h = plot(Xk(:, 1), Xk(:, 2), '.');
    set(h, 'Color', colors{k});
    
    items = itemNames(model.clusters==k);
    hold on;
    for i = 1:size(Xk, 1)
        t = text(Xk(i, 1), Xk(i, 2), items(i, :));
        set(t, 'Color', colors{k}); 
    end
end

end


% sources: http://www.mathworks.com/matlabcentral/fileexchange/
% 26184-em-algorithm-for-gaussian-mixture-model--em-gmm-/content/EmGm/logsumexp.m
function y = loggausspdf(X, mu, sigma)

d = size(X,1);
X = bsxfun(@minus,X,mu);
[U,p]= chol(sigma);
if p ~= 0
    error('ERROR: Sigma is not PD.');
end
Q = U'\X;
q = dot(Q,Q,1);  % quadratic term (M distance)
c = d*log(2*pi)+2*sum(log(diag(U)));   % normalization constant
y = -(c+q)/2;

end

function s = logsumexp(X, dim)

if nargin == 1, 
    % Determine which dimension sum will use
    dim = find(size(X)~=1,1);
    if isempty(dim), dim = 1; end
end

% subtract the largest in each dim
y = max(X,[],dim);
s = y+log(sum(exp(bsxfun(@minus,X,y)),dim));   % TODO: use log1p
i = isinf(y);
if any(i(:))
    s(i) = y(i);
end

end

