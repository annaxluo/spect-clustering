function [model] = objectSammon(X, p, y)
% reduces dimensionality using Sammon's mapping
% inputs: 
%   X - n-by-d data matrix
%   p - dimensionality of the reduced feature space
%   y - labels
% output: 
%   model - a model that shows the clusters of the training data; has
%       functions 'visualize', 'plot2D', and 'residvar' (compute
%       residual variance)


[n,d] = size(X);

% Compute all distances
D = X.^2*ones(d,n) + ones(n,d)*(X').^2 - 2*X*X';
D = sqrt(abs(D));

% Initialize low-dimensional representation with PCA
[U,S,V] = svd(X);
W = V(:,1:p)';
Z = X*W';

Z(:) = findMin(@stress,Z(:), 500, 0, D);

model.W = W; 
model.Z = Z; 
model.p = p; 
model.y = y; 
model.visualize = @visualize; 
model.plot2D = @plot2D; 
model.residvar = @residvar; 

end

function [f,g] = stress(Z, D)

n = length(D);
p = numel(Z)/n;
Z = reshape(Z, [n p]);

f = 0;
g = zeros(n, p);
for i = 1:n
    for j = i+1:n
        % Objective Function
        Dz = norm(Z(i,:)-Z(j,:));
        s = (D(i, j) - Dz)/sqrt(D(i, j)); 
        f = f + (1/2)*s^2; 
        
        % Gradient
        df = s/D(i, j);
        dgi = (Z(i, :)-Z(j, :))/Dz;
        dgj = (Z(j, :)-Z(i, :))/Dz; 
        % accumulate the change
        g(i,:) = g(i,:) - df*dgi;
        g(j,:) = g(j,:) - df*dgj;
    end
end
g = g(:);

end

function visualize(model)

% plot the reduced feature space
figure; 
imagesc(model.Z); colorbar; 
title('Reconstruction'); 
xlabel('p reduced features'); 
ylabel('n examples'); 

end

function plot2D(model)

figure; 
gscatter(model.Z(:, 1), model.Z(:, 2), model.y, [], ...
    'ox+*sdv^<>phox+*sd', [], 'on');
title('First 2 dimensions in k-space'); 
xlabel('D1'); 
ylabel('D2'); 
H = findobj(gcf, 'tag', 'legend'); 
set(H, 'location', 'eastOutside'); 

end

% compute residual variance
function r = residvar(model, X)
% distance matrix in original space and reduced space
Dx = distanceMatrix(X); 
Dz = distanceMatrix(model.Z); 

r = 1 - (corr(Dx(:), Dz(:)))^2; 

end





