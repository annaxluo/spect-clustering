# A Simple Toolbox for Exploratory Data Analysis

The PPMI SPECT dataset includes clinical and neuroimaging data of subjects 
with Parkinson's Disease (PD), subjects with motor deficits but no 
biomarkers of Parkinson's Disease (SWEDD), and healthy control (HC). In 
this collaborative coursework project, we explore multivariate analysis
algorithms to extract clinically significant information from this high-dimensional
dataset.

## Content of project
First, we tackle the challenging problem of classifying HC and SWEDD based on
neuroimaging data. Second, we demonstrate that clustering of neuroimaging data
can reveal distances between subjects that may help interpretation of imaging data
and prognosis. Finally, building upon clustering, we optimize regression algorithms
to predict clinical data from neuroimaging data.  

__My contribution to this project includes literature review (Section 1.1), clustering experiments (Section 3.2), and part of the discussion (Section 4).__ Please see the PDF **Coursework_report.pdf** for detailed report.

## Code
*Only my part of the code is included in this repositor. Not all codes included
here were used in the SPECT project.*

This code toolbox include the following functions: 

### 1. Dimensionality reduction:
`featNMF.m`: nonlinear matrix factorization <br />
`featSPCA.m`: sparse principal component analysis <br />
`objectPCA.m`: principal component analysis (via SVD) <br />
`objectMDS.m`: multidimensional scaling <br />
`objectISOMAP.m`: ISOMAP <br />
`objectSammon.m`: Sammon's mapping <br />

### 2. Clustering: 
`clusterKmeans.m`: k-means with random initialization <br />
`clusterKmeansPlusPlus.m`: k-means++ <br />
`clusterDBcluster.m`: DBSCAN <br />
`clusterGMM.m`: gaussian mixture model <br />

### 3. Examples: 
`example_obj_mds_clustering.m` and `examples_obj_pca_clustering.m`: 
These scripts demonstrate clustering cases after applying MDS or PCA to reduce 
the feature dimensionality. Three clustering methods used include k-means++, DBSCAN, and GMM. Final clusters are visualized. <br />
_Because sensitivity of the data, only a toy dataset is used in these examples._

## Partial Results
For full results, please see **Coursework_report.pdf**. 
Several clustering algorithms were able to discover clusters which created 
meaningful partition of the clinical labels (PD, SWEDD, and HC), and had 
significant difference in mean motor scores. 
<br><br>
Notably, after applying NMF (p=2) to the neuroimaging data, GMM (k=2) was able to 
identify one cluster that included 70.23% of all PD subjects, 11.48% of the HC 
subjects, and 7.5% of the SWEDD subjects. The other cluster included 29.77% of the 
PD subjects, 88.52% of the HC subjects, and 92.5% of the SWEDD subjects (Figure 13). The mean motor score was 18.75 (sd=0.47) for the first cluster, and 
8.81 (sd=0.43) for the second. 
<img src="https://gitlab.com/annaxluo/spect-clustering/uploads/157e0c0e5f3221bcfdd2a6d1044fc600/nmf_gmm.PNG" width=450 align="center">
<br><br>
Moreover, for every clinical label, its mean motor score in Cluster 1 was higher than that in Cluster 2, indicating that the HC/SWEDD subjects in Cluster 1 and PD subjects in Cluster 2 were not be due to
misplacement; they might have brain activity patterns that highly resembled those of the majority
label in the cluster. Clustering thus provides a tool for a more accurate prognosis of these “boundary
cases”. Finally, we note that most SWEDD subjects (74 out of 80) were placed in the cluster where
HC was the majority. This confirms the similarity in brain activity patterns between SWEDD and HC.
<br><br>
Another example output: 
<img src="https://gitlab.com/annaxluo/spect-clustering/uploads/a45398e0edb8c812b843c062a98660f8/nmf_kmeans.PNG" width=450 align="center">